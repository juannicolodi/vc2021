#Juan Ignacio Nicolodi UTN-FRC
#Ventana Alto=1[m] y Ancho=1.5[m]
#-----------------------------

import cv2
import numpy as np

#-----------------------------

blue = (255,0,0)
red = (0,255,0)
odx = [233, 237, 440, 436]
ody = [197, 342, 332, 203]
xbutton_down=[]
ybutton_down=[]
i=0
#---------------------------

def img(event,x,y,flags,param):
        global xbutton_down,ybutton_down,i    
        if(event==cv2.EVENT_LBUTTONDOWN):
                i=i+1
                cv2.line(img_perp,(x,y),(x,y),blue,4)
                xbutton_down.append(x)
                ybutton_down.append(y)
                if(i>=2):
                        cv2.line(img_perp,(xbutton_down[0],ybutton_down[0]),(xbutton_down[1],ybutton_down[1]),blue,2)
                        modulo = np.sqrt(((xbutton_down[0] - xbutton_down[1]) ** 2) + ((ybutton_down[0] - ybutton_down[1]) ** 2))
                        distancia = (modulo*1)/(ody[1]-ody[0])
                        font = cv2.FONT_ITALIC
                        cv2.putText(img_perp, str(distancia) , (int((xbutton_down[0]+xbutton_down[1])/2) , int((ybutton_down[0]+ybutton_down[1])/2)) , font , 1, red , 1,cv2.LINE_AA)
                        i=0
                        xbutton_down=[]
                        ybutton_down=[]
                        
                        
#---------------------------   

img_casa = cv2.imread('casa.jpg')      
imgc = np.array(img_casa) #Creamos una copia

cv2.namedWindow('Imagenn', cv2.WINDOW_NORMAL)#Nos perimite redimencionar el tamaño de la imagen al un valor deseado 
cv2.imshow('Imagenn',img_casa)

scr = np.float32([[odx[0],ody[0]],[odx[1],ody[1]],[odx[2],ody[2]],[odx[3],ody[3]]])
 
Alto_AB = np.sqrt(((odx[0] - odx[1]) ** 2) + ((ody[0] - ody[1]) ** 2))
Alto_CD = np.sqrt(((odx[2] - odx[3]) ** 2) + ((ody[2] - ody[3]) ** 2))
maxAlto = max(int(Alto_AB), int(Alto_CD))#Nos quedamos con el mayor de los dos
maxAncho =  maxAlto*1.5 #Para mantener la relacion de que el alto es 1m y el ancho es 1.5m

dst = np.float32([[odx[0], ody[0]],[odx[0],ody[0] + maxAlto - 1],[odx[0]+maxAncho - 1, ody[0]+maxAlto - 1],[odx[0]+maxAncho - 1, ody[0]]]) #Coordenadas Desntino

M = cv2.getPerspectiveTransform(scr,dst)

img_perp = cv2.warpPerspective(imgc,M,(img_casa.shape[1]+500, img_casa.shape[0]+300),flags=cv2.INTER_LINEAR)
            
cv2.imwrite('RectificacionCompleta.jpg',img_perp)
cv2.namedWindow('ImagenPerspec', cv2.WINDOW_NORMAL)
cv2.imshow('ImagenPerspec', img_perp)
print('Presione cualquier tecla para continuar')
cv2.waitKey()
cv2.destroyWindow('Imagenn')

cv2.setMouseCallback('ImagenPerspec',img)
imgcpersp = np.array(img_perp) #Creamos una copia

print('Toque dos puntos para determinar distancia')
print('Presione r para restaurar o q para salir')
while True:
        cv2.imshow('ImagenPerspec', img_perp)
        k= cv2.waitKey(1) & 0xFF

        if(k == ord('r')):
                img_perp= np.array(imgcpersp) #Creamos una copia
        
        if(k == ord('q')):
                break

#Los valores obtenidos mientras nos alejasmos de los puntos rectificados empizan a contener un poco de error
#Dentro de todo lo valores bastante exacto
#-----------------------------
