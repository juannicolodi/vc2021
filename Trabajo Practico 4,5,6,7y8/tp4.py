import cv2
import numpy as np
import mymodulo2 as m #Modulo propio donde se encuentran las funciones del tp5,6,7y8

#-------------------------------------------- 

blue = (255,0,0) ; green = (0,255,0) ; red = (0,0,255)
drawing = False 
xybutton_down = -1,-1
xybutton_up = -1,-1
g=0 #Controla que el Rectangulo este dibujado

#-----------------------------------------
def dibuja(event,x,y,flags,param):

    global xybutton_down, xybutton_up, drawing, g, img

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        xybutton_down = x,y
    
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            img = np.array(imgcopia)
            xybutton_up = x,y
            cv2.rectangle(img,xybutton_down,(x,y),blue,2)           
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        g=1

#--------------------------------------------------------

img = cv2.imread('hoja.jpg')
imgcopia = np.array(img)

cv2.namedWindow ('image')
cv2.setMouseCallback('image',dibuja)

while(True):
    
    cv2.imshow('image',img)
    
    k = cv2.waitKey(1) & 0xFF
    
    if(k == ord('q')):#Salir
        break
   
    if(k==ord('g')):#Recortar y guardar 
        if(g==1):
            img_g = imgcopia [ xybutton_down[1] : xybutton_up[1] , xybutton_down[0] : xybutton_up[0] ]
            cv2.imwrite('recorte.jpg',img_g)
            print('Imagen Guardada')
        else:
            print('No hay rectangulo dibujado')
    
    if(k==ord('r')):# Restaurar
        img = np.array(imgcopia)
        g=0
        print('Imagen restaurada')
    
    if(k==ord('e') and g==1): #Transformación euclideana
        img_g = imgcopia [ xybutton_down[1] : xybutton_up[1] , xybutton_down[0] : xybutton_up[0] ]
        m.euclideana(img_g,10,100,100,(0,0))

    if(k==ord('s') and g==1): #Transformación similarida
        img_g = imgcopia [ xybutton_down[1] : xybutton_up[1] , xybutton_down[0] : xybutton_up[0] ]
        m.similaridad(img_g,10,100,100,0.5,(0,0))

    if(k==ord('a')): #Transformación afin
        cv2.destroyWindow('image')
        img_g= cv2.imread('hoja.jpg')
        m.afin(img_g)
        cv2.namedWindow ('image')
        cv2.setMouseCallback('image',dibuja)

    if(k==ord('h')): #Transformación perspectiva
        cv2.destroyWindow('image')
        img_h = cv2.imread('Celu.jpg')
        m.persp(img_h)
        cv2.namedWindow ('image')
        cv2.setMouseCallback('image',dibuja)

    
            
        
cv2.destroyAllWindows()
