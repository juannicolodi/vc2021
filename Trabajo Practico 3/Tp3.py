#Juan Ignacio Nicolodi UTN-FRC VC2021
#----------------------------------------

import sys
import cv2

#----------------------------------------

if(len(sys.argv)>1):
    filename = sys.argv[1]
else:
    print('Pasar el nombre del video como argumento ')
    sys.exit(0)

cap = cv2.VideoCapture(filename)

if(cap.isOpened()):
    ret, frame = cap.read()
    h, w = frame.shape[0:2] #Ancho y alto  de la imagen
    framespersecond= int(cap.get(cv2.CAP_PROP_FPS)) #Funcion que toma la cantidad de FPS de cap
    delay=1000//framespersecond

print('Alto={0} Ancho={1}'.format(h,w))
print('fps={0} y delay={1}[ms]'.format(framespersecond,delay))

fourcc = cv2.VideoWriter_fourcc('X' , 'V' , 'I' , 'D')
out = cv2.VideoWriter('output.avi', fourcc , framespersecond , (w,h))


while(cap.isOpened()):

    ret,frame = cap.read()
    
    if ret is True:
        gray = cv2.cvtColor(frame , cv2.COLOR_BGR2GRAY)#El video obtenido no se puede reproducir por esta linea, si se comenta crea bien el video.
        out.write(gray)
        cv2.imshow('frame',gray)
        if cv2.waitKey(delay) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows ( )
