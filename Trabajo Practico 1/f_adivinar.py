#Trabajo Práctico 1 VC 2021 Juan Ignacio Nicolodi

#------------------------------

import random

#------------------------------

def adivinar(N): # N = Numero de intentos 
    
    numero = random.randint(0,100) # Generación de número aleatorio
    i=1 #Índice número de intentos
    
    while(True):
        
        if(i>N):
            print('Supero el numero de intentos')
            break
        
        valor= int(input('Ingrese un valor: ')) #Con el int convertido el str de la entrada

        if(valor==numero):
            print('Acerto, en el intento {0}'.format(i))
            break
        elif(valor>numero):
            print('Es un poco menor')
        else:
            print('Es un poco mayor')
        
        i+=1

#------------------------------

adivinar(10)
